$(document).ready(function() {
    $('#likes').click(function() {
        var catid;
        catid = $(this).attr("data-catid");
        $.get('/rango/like/', {category_id: catid}, function(data){
            $('#like_count').html(data);
            $('#likes').hide();
        });
    }); //Click end

    $('#suggestion').keyup(function(){
        var query;
        query = $(this).val();
        $.get('/rango/suggest', {suggestion: query}, function(data){
            $('#cats').html(data);
        }); //End get ajax
    }); //End keyup

    $('.rango-add').click(function(){
        var catid = $(this).attr("data-catid");
        var url = $(this).attr("data-url");
        var title = $(this).attr("data-title");
        var me = $(this)
        $.get('/rango/add/',
            {category_id: catid, url: url, title: title}, function(data) {
            $('#pages').html(data);
            me.hide();
        }); //End rango-add get ajax
    }); //End rango-add click

}); //Ready end