from django.conf.urls import url
from rango import views


app_name = 'rango'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^about/?$', views.about, name='about'),
    url(r'^add_category/$', views.AddCategory.as_view(), name='add_category'),
    url(r'^category/(?P<category_name_slug>[\w\-]+)/?$',
        views.category, name='category'),
    url(r'^category/(?P<category_name_slug>[\w\-]+)/add_page/$',
        views.AddPage.as_view(), name='add_page'),
    url(r'^add/$', views.auto_add_page, name='auto_add_page'),
    url(r'^like/$', views.like_category, name='like_category'),
    url(r'^suggest/$', views.suggest_category, name='suggest_category'),
    url(r'^register/$', views.register, name='register'),
    url(r'^register_profile/$', views.RegisterProfile.as_view(),
        name='register_profile'),
    url(r'^profile/(?P<username>[\w\-]+)/$',
        views.Profile.as_view(), name='profile'),
    url(r'^profiles/$', views.list_profiles, name='list_profiles'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^restricted/$', views.restricted, name='restricted'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^goto/$', views.track_url, name='goto'),
]
