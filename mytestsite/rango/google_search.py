from apiclient.discovery import build
from configparser import ConfigParser


def get_keys(file='api.ini'):
    config = ConfigParser()
    config.read(file)
    api = {'api_key': config['google']['api_key'],
           'engine_id': config['google']['engine_id']}
    return api


def run_search(search_term):
    api = get_keys()
    if not api:
        raise KeyError('Google api keys not found!')
    results = []
    try:
        service = build('customsearch', 'v1', developerKey=api['api_key'])
        results = service.cse().list(
            q=search_term,
            cx=api['engine_id']
        ).execute()
        return results['items']
    except Exception:
        print("Error when querying the Google API")
    return results
