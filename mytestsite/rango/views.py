from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.generic import View
from django.utils import timezone
from datetime import datetime
from rango.models import Category, Page, UserProfile
from rango.forms import CategoryForm, PageForm, UserForm, UserProfileForm
from rango.google_search import run_search


def index(request):
    request.session.set_test_cookie()
    category_list = Category.objects.order_by('-likes')[:5]
    pages_list = Page.objects.order_by('-views')[:5]
    context_dict = {'categories': category_list, 'top_pages': pages_list}
    visitor_cookie_handler(request)
    context_dict['visits'] = request.session['visits']
    response = render(request, 'rango/index.html', context_dict)
    return response


def about(request):
    request.session.set_test_cookie()
    visitor_cookie_handler(request)
    context_dict = {'bmess': 'Artem', 'visits': request.session['visits']}
    return render(request, 'rango/about.html', context_dict)


def category(request, category_name_slug):
    context_dict = {}
    try:
        category = Category.objects.get(slug=category_name_slug)
        context_dict['category_name'] = category.name

        pages = Page.objects.filter(category=category).order_by('-views')
        context_dict['pages'] = pages

        context_dict['category'] = category
    except Category.DoesNotExist:
        context_dict['category'] = None
        context_dict['pages'] = None
        context_dict['category_name'] = None

    context_dict['query'] = context_dict['category_name']
    results = []
    if request.method == 'POST':
        query = request.POST['query'].strip()
        if query:
            results = run_search(query)
            context_dict['query'] = query
            context_dict['results'] = results

    return render(request, 'rango/category.html', context_dict)


def get_category_list(max_results=0, starts_with=''):
    cat_list = []
    if starts_with:
        cat_list = Category.objects.filter(name__istartswith=starts_with)

    if max_results > 0:
        if len(cat_list) > max_results:
            cat_list = cat_list[:max_results]

    return cat_list


def suggest_category(request):
    cat_list = []
    starts_with = ''

    if request.method == 'GET':
        starts_with = request.GET.get('suggestion', '')
    cat_list = get_category_list(8, starts_with)

    return render(request, 'rango/cats.html', {'cats': cat_list})


@login_required
def like_category(request):
    cat_id = None
    if request.method == 'GET':
        cat_id = request.GET.get('category_id', None)
        likes = 0
        if cat_id:
            cat = Category.objects.get(id=int(cat_id))
            if cat:
                likes = cat.likes + 1
                cat.likes = likes
                cat.save()

        return HttpResponse(likes)


def track_url(request):
    page_id = None
    url = reverse('rango:index')
    if request.method == 'GET':
        if 'page_id' in request.GET:
            page_id = request.GET['page_id']

            try:
                page = Page.objects.get(id=page_id)
                page.views += 1
                page.last_visit = timezone.now()
                page.save()
                url = page.url
            except Exception:
                pass
    return redirect(url)


def search(request):
    results = []
    query = ''
    if request.method == 'POST':
        query = request.POST['query'].strip()
        if query:
            results = run_search(query)
    return render(request, 'rango/search.html', {
        'results': results,
        'query': query
    })


@method_decorator(login_required, name='dispatch')
class AddCategory(View):
    form_class = CategoryForm
    template_name = 'rango/add_category.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return index(request)
        else:
            return render(request, self.template_name, {'form': form})


@login_required
def auto_add_page(request):
    cat_id = None
    url = None
    title = None
    context_dict = {}
    if request.method == 'GET':
        cat_id = request.GET.get('category_id', None)
        url = request.GET.get('url', None)
        title = request.GET.get('title', None)
        if cat_id and url and title:
            category = Category.objects.get(id=int(cat_id))
            p = Page.objects.get_or_create(
                category=category, title=title, url=url)
            pages = Page.objects.filter(category=category).order_by('-views')
            context_dict['pages'] = pages

    return render(request, 'rango/page_list.html', context_dict)


class AddPage(View):
    form_class = PageForm
    template_name = 'rango/add_page.html'
    category_name_slug = None

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.category_name_slug = kwargs.get('category_name_slug', None)
        try:
            self.category = Category.objects.get(slug=self.category_name_slug)
        except Category.DoesNotExist:
            self.category = None
        return super(AddPage, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        context_dict = {'form': form,
                        'category': self.category,
                        'category_name_slug': self.category_name_slug
                        }
        return render(request, self.template_name, context_dict)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid() and self.category:
            page = form.save(commit=False)
            page.category = self.category
            page.views = 0
            page.save()
            return HttpResponseRedirect(reverse(
                'rango:category',
                kwargs={'category_name_slug': self.category_name_slug}))
        else:
            return render(request, self.template_name,
                          {'form': form, 'category': self.category})


def register(request):
    registered = False

    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']
                profile.save()
                registered = True
        else:
            print(user_form.errors, profile_form.errors)
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    return render(request, 'rango/register.html',
                  {'user_form': user_form,
                   'profile_form': profile_form,
                   'registered': registered
                   }
                  )


class Profile(View):
    form_class = UserProfileForm
    template_name = 'rango/profile.html'
    username = None

    def dispatch(self, request, *args, **kwargs):
        try:
            self.username = kwargs.get('username', None)
            self.user = User.objects.get(username=self.username)
        except User.DoesNotExist:
            return redirect('rango:index')

        self.userprofile = UserProfile.objects.get_or_create(user=self.user)[0]

        return super(Profile, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = self.form_class(
            {'website': self.userprofile.website,
             'picture': self.userprofile.picture}
        )
        context = {
            'form': form,
            'selecteduser': self.user,
            'userprofile': self.userprofile
        }
        return render(request, self.template_name, context)

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        form = self.form_class(
            request.POST,
            request.FILES,
            instance=self.userprofile
        )
        if form.is_valid():
            form.save(commit=True)
            return redirect('rango:profile', self.user.username)
        else:
            context = {
                'form': form,
                'selecteduser': self.user,
                'userprofile': self.userprofile
            }
            return render(request, self.template_name, context)


def list_profiles(request):
    userprofile_list = UserProfile.objects.all()
    return render(request,
                  'rango/list_profiles.html',
                  {'userprofile_list': userprofile_list}
                  )


@method_decorator(login_required, name='dispatch')
class RegisterProfile(View):
    form_class = UserProfileForm
    template_name = 'rango/profile_registration.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        try:
            profile = request.user.userprofile
        except UserProfile.DoesNotExist:
            profile = UserProfile(user=request.user)

        form = self.form_class(request.POST, request.FILES, instance=profile)
        if form.is_valid():
            user_profile = form.save(commit=False)
            user_profile.save()
            return redirect('rango:index')
        else:
            return render(request, self.template_name, {'form': form})


def user_login(request):
    error = False
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('rango:index'))
            else:
                error = "Your rango's account is disabled!"
        else:
            error = "Invalid login details: \
                    {0}, {1}".format(username, password)
    return render(request, 'rango/login.html', {'error': error})


@login_required
def restricted(request):
    return render(request, 'rango/restricted.html', {})


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('rango:index'))


def get_server_side_cookie(request, cookie, default_val=None):
    val = request.session.get(cookie)
    if not val:
        val = default_val
    return val


def visitor_cookie_handler(request):
    visits = int(get_server_side_cookie(request, 'visits', '1'))
    last_visit_cookie = get_server_side_cookie(request,
                                               'last_visit',
                                               str(datetime.now()))
    last_visit_time = datetime.strptime(last_visit_cookie[:-7],
                                        '%Y-%m-%d %H:%M:%S')
    if (datetime.now() - last_visit_time).days > 0:
        visits += 1
        request.session['last_visit'] = str(datetime.now())
    else:
        visits = 1
        request.session['last_visit'] = last_visit_cookie
    request.session['visits'] = visits
