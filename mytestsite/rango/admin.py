from django.contrib import admin
from rango.models import Category, Page, UserProfile


class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'url')
    list_filter = ['category']
    fieldsets = [
        ('Information data', {'fields': ['title', 'url']}),
        (None, {'fields': ['category']}),
        ('Views', {'fields': ['views'], 'classes': ['collapse']})
    ]


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Category, CategoryAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(UserProfile)
