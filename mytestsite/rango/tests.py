from django.test import TestCase
from django.core.urlresolvers import reverse
from django.utils import timezone
from rango.models import Category, Page


class CategoryMethodTests(TestCase):
    def test_ensure_views_are_positive(self):
        cat = Category(name='test', views=1, likes=0)
        cat.save()
        self.assertEqual((cat.views >= 0), True)

    def test_slug_line_creation(self):
        cat = Category(name='Random Category string')
        cat.save()
        self.assertEqual(cat.slug, 'random-category-string')


class PageMethodTests(TestCase):
    def test_ensure_visits_not_future(self):
        cat = add_cat(name='test')
        page = Page(category=cat, title='test page')
        page.save()
        self.assertEqual((page.first_visit < timezone.now()), True)
        self.assertEqual((page.last_visit < timezone.now()), True)

    def test_compare_last_and_first_visits(self):
        cat = add_cat(name='test1')
        page = Page(category=cat, title='test page1')
        page.save()
        self.assertEqual((page.first_visit <= page.last_visit), True)

class IndexViewTests(TestCase):
    def test_index_view_with_no_categories(self):
        response = self.client.get(reverse('rango:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'There are no categories present')
        self.assertQuerysetEqual(response.context['categories'], [])

    def test_index_view_with_no_categories(self):
        add_cat('test')
        add_cat('temp', 1, 1)
        add_cat('tmp', 1, 1)
        add_cat('Cat is cool')
        response = self.client.get(reverse('rango:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Cat is cool')
        num_cats = len(response.context['categories'])
        self.assertEqual(num_cats, 4)


def add_page(cat, title, url='', views=0):
    page = Page.objects.get_or_create(
        category=cat,
        title=title,
        url=url,
        views=views,
        first_visit=timezone.now(),
        last_visit=timezone.now()
    )
    page.save()
    return page


def add_cat(name, views=0, likes=0):
    cat = Category.objects.get_or_create(name=name)[0]
    cat.views = views
    cat.likes = likes
    cat.save()
    return cat
